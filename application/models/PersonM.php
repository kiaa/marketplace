<?php

// extends class Model
class PersonM extends CI_Model{

  // response jika field ada yang kosong
  public function empty_response(){
    $response['status']=502;
    $response['error']=true;
    $response['message']='Field tidak boleh kosong';
    return $response;
  }

  // function untuk insert data ke tabel tb_person
  public function add_Absences($name,$date,$kelas,$subjects,$information){

    if(empty($name) || empty($date) || empty($kelas) || empty($subjects) || empty($information)) {
      return $this->empty_response();
    }else{
      $data = array(
        "name"=>$name,
        "date"=>$date,
        "kelas"=>$kelas,
        "subjects"=>$subjects,
        "information"=>$information
      );

      $insert = $this->db->insert("tb_absent", $data);

      if($insert){
        // $response['status']=200;
        // $response['error']=false;
        // $response['message']='Data person ditambahkan.';
        $response = $data;
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='addition failed, try again!';
        return $response;
      }
    }

  }

  // mengambil semua data person
  public function all_Absences(){

    $all = $this->db->get("tb_absent")->result();
    // $response['status']=200;
    // $response['error']=false;
    $response=$all;
    return $response;

  }

  // hapus data person
  public function delete_Absences($id){

    if($id == ''){
      return $this->empty_response();
    }else{
      $where = array(
        "id"=>$id
      );

      $this->db->where($where);
      $delete = $this->db->delete("tb_absent");
      if($delete){
        $response['status']=200;
        $response['error']=false;
        $response['message']=' delete successfully';
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='delete failed, try again!';
        return $response;
      }
    }

  }

  // update person
  public function update_Absences($id,$name,$date,$kelas,$subjects,$information){

    if($id == '' || empty($name) || empty($date) || empty($kelas) || empty($subjects) || empty($information)){
      return $this->empty_response();
    }else{
      $where = array(
        "id"=>$id
      );

      $set = array(
        "name"=>$name,
        "date"=>$date,
        "kelas"=>$kelas,
        "subjects"=>$subjects,
        "information"=>$information
      );

      $this->db->where($where);
      $update = $this->db->update("tb_absent",$set);
      if($update){
        // $response['status']=200;
        // $response['error']=false;
        // $response['message']='Data person diubah.';
        $response = $where+$set;
        return $response;
      }else{
        $response['status']=502;
        $response['error']=true;
        $response['message']='Update failed, try again!';
        return $response;
      }
    }

  }

}

?>
