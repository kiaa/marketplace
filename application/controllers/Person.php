<?php

require APPPATH . 'libraries/REST_Controller.php';

class Person extends REST_Controller{

  // construct
  public function __construct(){
    parent::__construct();
    $this->load->model('PersonM');
  }

  // method index untuk menampilkan semua data person menggunakan method get
  public function index_get(){
    $response = $this->PersonM->all_Absences();
    $this->response($response);
  }

  // untuk menambah person menaggunakan method post
  public function add_post(){
    $response = $this->PersonM->add_Absences(
        $this->post('name'),
        $this->post('date'),
        $this->post('kelas'),
        $this->post('subjects'),
        // $this->post('image'),
        $this->post('information')
      );
    $this->response($response);
  }

  // update data person menggunakan method put
  public function update_put(){
    $response = $this->PersonM->update_Absences(
        $this->put('id'),
        $this->put('name'),
        $this->put('date'),
        $this->put('kelas'),
        $this->put('subjects'),
        // $this->put('image'),
        $this->put('information')
      );
    $this->response($response);
  }

  // hapus data person menggunakan method delete
  public function delete_delete(){
    $response = $this->PersonM->delete_Absences(
        $this->delete('id')
      );
    $this->response($response);
  }

}

?>
